##########################################################
django-rules-examples - Real code examples of django-rules
##########################################################

Some real code examples using `django-rules
<https://github.com/dfunckt/django-rules>`_.

The project README covers the concepts of the app fairly well but the examples
are really light on details. This repository attempts to cover the different
use cases presented in the README in different branches.

Getting started
===============

.. code:: sh

   $ pip install -r requirements.txt
   $ cd blackbook
   $ python manage.py migrate
   $ python manage.py loaddata fixtures.json

Fixtures
========

.. code:: sh

   $ python manage.py dumpdata auth books --natural-foreign --natural-primary -e auth.Permission

License
=======

django-rules-examples is published under a BSD 3-clause license, see the
LICENSE file distributed with the project.
