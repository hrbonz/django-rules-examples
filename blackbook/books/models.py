from django.conf import settings
from django.db import models

import rules
from rules.contrib.models import RulesModel

from books.rules import is_editor, is_book_author, is_book_author_or_editor


# Create your models here.

class Book(RulesModel):
    isbn = models.CharField(max_length=17)
    title = models.CharField(max_length=255)
    author = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete=models.CASCADE,
    )

    class Meta:
        rules_permissions = {
            "add": is_editor,
            "read": rules.is_authenticated,
            "change": is_book_author_or_editor,
            "delete": is_book_author,
        }
