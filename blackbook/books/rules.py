import rules


@rules.predicate
def is_book_author(user, book):
    return book.author == user

is_editor = rules.is_group_member('editors')

is_book_author_or_editor = is_book_author | is_editor

rules.add_rule('can_edit_book', is_book_author_or_editor)
rules.add_rule('can_delete_book', is_book_author)
